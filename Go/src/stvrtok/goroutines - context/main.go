package main

import (
	"context"
	"fmt"
)

func main() {
	// gen generates integers in a separate goroutine and
	// sends them to the returned channel.
	// The callers of gen need to cancel the context once
	// they are done consuming generated integers not to leak
	// the internal goroutine started by gen.
	gen := func(ctx context.Context) <-chan int {
		// create a channel and store it to variable dst
		dst := make(chan int)
		// initialize variable n with 1
		n := 1
		// execute function as go routine
		go func() {
			// infinite for loop
			for {
				// select for choosing between channel options
				select {
				// case if writing to channel is done
				case <-ctx.Done():
					return // returning not to leak the goroutine
				// case if still writing to channel
				case dst <- n:
					//then increment value in variable n
					n++
				}
			}
		}()
		//at the end return values from channel dst
		return dst
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished consuming integers

	for n := range gen(ctx) {
		fmt.Println(n)
		if n == 5 {
			break
		}
	}
}
