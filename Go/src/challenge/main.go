package main

import "fmt"

// struct to store full name
type Account struct {
	firstName, lastName string
}

// struct to store credits
type Employee struct {
	credits float64
	Account
}

// method to change names in Account struct via Employee struct
func (a *Employee) changeName(fn, ln string) {
	a.Account.firstName = fn
	a.Account.lastName = ln
}

// method to return first and last name from Account struct via Employee struct
func (e Account) String() string {
	return fmt.Sprintf("First Name: %v\nLast Name: %v", e.firstName, e.lastName)
}

// method to add credits to Employee struct
func (c *Employee) AddCredits(credits float64) {
	c.credits = c.credits + credits
}

// method to deduct credits in Employee struct
func (d *Employee) RemoveCredits(credits float64) {
	d.credits = d.credits - credits
}

// method to deduct credits in Employee struct
func (f *Employee) CheckCredits() float64 {
	return f.credits
}

func main() {
	// set name through variable
	name := Employee{0.0, Account{"John", "Doe"}}
	// print name to console
	fmt.Println(name)
	// change name in struct Account via Employee struct
	name.changeName("Andrej", "Danko")
	// print change name
	fmt.Println(name)
	// set amount of credits to add
	name.AddCredits(159.1)
	// print amount of credits
	fmt.Printf("Amount of credits: %.2f\n", name.credits)
	// set amount of credits to deducy
	name.RemoveCredits(59.1)
	// print new amount of credits
	fmt.Printf("New amount of credits after deduction: %.2f\n", name.credits)
	// just print amount of credits using CheckCredits
	fmt.Printf("Credits: %.2f", name.CheckCredits())
}
