package main

import (
	"fmt"
	"strings"
)

var input = make(map[string]string)

func main() {
	ch := make(chan string, len(input)*3)
	var str string

	fmt.Println("Write your string:")
	fmt.Scanln(&str)

	go lower(str, ch)
	go upper(str, ch)
	go append(str, ch)
	for i := 0; i < 3; i++ {
		fmt.Println(<-ch)
	}
}

func lower(str string, ch chan string) {
	input[str] = strings.ToLower(str)
	ch <- input[str]
}

func upper(str string, ch chan string) {
	input[str] = strings.ToUpper(str)
	ch <- input[str]
}

func append(str string, ch chan string) {
	//fmt.Println("Your string with little more: " + str + " " + "This is a test")
	input[str] = str + " " + "This is a test"
	ch <- input[str]
}
