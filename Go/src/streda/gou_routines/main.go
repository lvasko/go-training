package main

import (
	"fmt"
	"time"
)

var num1, num2 int
var start = time.Now()
var resultChan = make(chan int, 6)

func main() {
	num1, num2 = 8, 2

	go add(num1, num2, resultChan)
	go substract(num1, num2, resultChan)
	go multiply(num1, num2, resultChan)
	go division(num1, num2, resultChan)
	go expo(num1, num2, resultChan)
	go constant(num1, num2, resultChan)
	time.Sleep(200 * time.Microsecond)

	for i := 0; i < 6; i++ {
		fmt.Println(<-resultChan)
	}

	elapsed := time.Since(start)
	fmt.Println("Time elapsed since start:", elapsed)
}

func add(num1, num2 int, ch chan int) {
	result := num1 + num2
	ch <- result
}

func substract(num1, num2 int, ch chan int) {
	result1 := num1 - num2
	ch <- result1
}

func multiply(num1, num2 int, ch chan int) {
	result2 := num1 * num2
	ch <- result2
}

func division(num1, num2 int, ch chan int) {
	result3 := num1 / num2
	ch <- result3
}

func expo(num1, num2 int, ch chan int) {
	result4 := num1 ^ num2
	ch <- result4
}

func constant(num1, num2 int, ch chan int) {
	result5 := num1 + num2 + 20
	ch <- result5
}
