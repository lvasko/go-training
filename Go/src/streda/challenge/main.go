package main

import (
	"fmt"
	"time"
)

// initialize map with keys and values of strings
var map_2 = map[string]string{
	"Dominik": "Domino",
	"Jozef":   "Jozo",
	"Marek":   "Maro",
}

// initialize array of Names with strings
//var names = []string{"Dominik", "Jozef", "Marek"}

func main() {
	var name string
	/*
		// iterate through length of array names
		for i := 0; i < len(names); i++ {
			// pass values from indexes of names array to function read and run it as go routine
			go read(names[i])
			// sleep for 200 milisecond to give time to go routine to run
			time.Sleep(200 * time.Millisecond)
		}
	*/

	fmt.Println("Write a name to display a nickname:")
	fmt.Scanln(&name)

	go read(name)
	time.Sleep(200 * time.Millisecond)

}

// declare function read which takes variable name of type string
func read(name string) {
	// pass value from variable name to map map_2 and print matching value from map key
	fmt.Println(name, "has a nickname:", map_2[name])
}
