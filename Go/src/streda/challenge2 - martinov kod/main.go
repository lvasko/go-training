package main

import (
	"fmt"
)

var nicknames = make(map[string]string)

func main() {
	nicknames["ivan"] = "ivo"
	nicknames["robert"] = "robo"
	nicknames["peter"] = "peto"

	ch := make(chan string, len(nicknames))

	go findNickname("ivan", ch)
	go findNickname("robert", ch)
	go findNickname("peter", ch)

	for i := 0; i < len(nicknames); i++ {
		fmt.Println(<-ch)
	}
}

func findNickname(nick string, ch chan string) {
	ch <- nicknames[nick]
}
