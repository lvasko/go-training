package main

import (
	"fmt"
	"math/rand"
	"time"
)

// function to calculate fibonnaci numbers
func fib(number float64, ch chan string) {
	// initialize variable for calculating
	x, y := 1.0, 1.0
	// iterate through number variable and calculate fibonaci
	for i := 0; i < int(number); i++ {
		x, y = y, x+y
	}

	// generate random number form 1 - 3
	r := rand.Intn(3)
	// sleep for seconds from generated number
	time.Sleep(time.Duration(r) * time.Second)

	// send fibonaci number to chanell
	ch <- fmt.Sprintf("Fib(%v): %v\n", number, x)
}

func main() {
	// store starting time in variable
	start := time.Now()

	// set size for channell
	size := 15
	// create a channell with size of 15
	ch := make(chan string, size)

	// iterate through size of a channell
	for i := 0; i < size; i++ {
		// generate a fibonaci number for every iteration
		go fib(float64(i), ch)
	}

	// print data from channell based on size of a channell
	for i := 0; i < size; i++ {
		fmt.Printf(<-ch)
	}

	// store how much time elapsed from starting a program
	elapsed := time.Since(start)
	// print how much time elapsed
	fmt.Printf("Done! It took %v seconds!\n", elapsed.Seconds())
}
